import mysql.connector


class Resource:
    def __init__(self):
        self.cur = None
        self.cnx: mysql.connector.MySQLConnection = None

    def get_db_cursor(self):
        if not self.cur:
            self.cur = self.cnx.cursor(dictionary=True)
        return self.cur

    def connect_db(self):
        self.cnx = mysql.connector.connect(user='root', password='root', host='127.0.0.1')

    def close_db_connection(self):
        if self.cur is not None:
            self.cur.close()
        self.cnx.close()
