from bus import __add_bus__
from reservation import __add_reservation__
from resource import Resource


def test__add_reservation__():
    __add_bus__(_resource, name='bus for test reservation 1', start='start point', stop='test stop point', fare=90,
                seats=100)
    __add_bus__(_resource, name='bus for test reservation 2', start='another start point', stop='another stop point',
                fare=90, seats=50)
    __add_reservation__(_resource, 'customer 1', 50, 'bus for test reservation 1')
    __add_reservation__(_resource, 'customer 2', 50, 'bus for test reservation 1')
    __add_reservation__(_resource, 'customer 1', 100, 'bus for test reservation 2')


if __name__ == '__main__':
    _resource = Resource()
    _resource.connect_db()
    try:
        test__add_reservation__()
    finally:
        _resource.close_db_connection()