from bus import __add_bus__, __search_bus__
from resource import Resource


def test__add_bus__():
    __add_bus__(_resource, name='test_bus', start='test_start_point', stop='test stop point', fare=90, seats=100)
    __add_bus__(_resource, name='bus one', start='start one', stop='stop one', fare=90, seats=100)
    __add_bus__(_resource, name='bus two', start='start one', stop='stop one', fare=90, seats=100)


def test__search_bus__():
    cur = __search_bus__(_resource, name='test_bus', start='test_start_point', stop='test stop point')
    results = cur.fetchall()
    assert len(results) == 1, f'{len(results)=}'
    for i in results:
        assert 'test_bus' == i['name']
        assert 'test_start_point' == i['start']
        assert 'test stop point' == i['stop']
        assert 90 == i['fare']
        assert 100 == i['seats']

    cur = __search_bus__(_resource, start='start one')
    results = cur.fetchall()
    assert len(results) == 2, f'{len(results)=}'
    for i in results:
        assert 'start one' == i['start'], f"{i['start']=}"


if __name__ == '__main__':
    _resource = Resource()
    _resource.connect_db()
    try:
        test__add_bus__()
        test__search_bus__()
    finally:
        _resource.close_db_connection()
