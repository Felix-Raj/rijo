import logging

import mysql.connector

import queries
from exceptions import AppException

cols = ['bus name', 'boarding point', 'dropping point', 'fare', 'seats']  # to show in heading
col_order = ['name', 'start', 'stop', 'fare', 'seats']  # cols to show, name as in db col name


def __add_bus__(resource, **kwargs):
    cur = resource.get_db_cursor()
    try:
        cur.execute(queries.insert_into_bus, (kwargs['name'], kwargs['start'], kwargs['stop'], kwargs['fare'],
                                              kwargs['seats']))
        resource.cnx.commit()
    except mysql.connector.Error as err:
        logging.error(f'error inserting data into {queries.table_bus_info}: {err}')
        raise AppException(f"failed to add bus {kwargs['name']}")


def __search_bus__(resource, start='', stop='', name=''):
    w = {
        'start': start,
        'stop': stop,
        'name': name,
    }

    q = queries.select_bus_partial + ' and '.join([f'{k}=%s' for k, v in w.items() if v and not v.isspace()])
    args = (v for k, v in w.items() if v and not v.isspace())
    args = tuple(args)
    logging.debug(f'{q=}')
    cur = resource.get_db_cursor()
    try:
        cur.execute(q, args)
    except mysql.connector.Error as err:
        m = f'error querying bus for start={start}, stop={stop}, name={name}'
        logging.error(f'{m}: {err}')
        raise AppException(m)
    return cur


def __get_all_bus(resource):
    cur = resource.get_db_cursor()
    try:
        cur.execute(queries.select_all_bus)
    except mysql.connector.Error as err:
        m = f'error getting list of all buses\n' \
            f'err: {err}\n' \
            f'query: {cur.statement}'
        logging.error(m)
        raise AppException(m)
    return cur


def add_bus(resource):
    inputs_prompt = {
        'name': 'bus name (should be unique)',
        'start': 'boarding point',
        'stop': 'dropping point',
        'fare': 'fare',
        'seats': 'seats',
    }
    values = {}
    for k, v in inputs_prompt.items():
        values[k] = input(f'>> {v.title()}: ')
    __add_bus__(resource, **values)


def search_bus(resource):
    input_prompt = {
        'start': 'boarding point',
        'stop': 'dropping point',
        'name': 'name'
    }
    values = {}
    for k, v in input_prompt.items():
        values[k] = input(f'>> {v.title()}: ')
    cur = __search_bus__(resource, **values)

    # todo: show remaining seats instead of total seats

    print(f"searched for bus with"
          f" {', '.join(f'{input_prompt[k].title()}: {v}' for k,v in values.items() if v and not v.isspace())}".capitalize())
    print_title()
    for row in cur.fetchall():
        print_row(row)
    print_seperator()


def print_row(row):
    print(f"| {' | '.join(f'{str(row[x])[0:19]:^20}' for x in col_order)} |")
    rows_required = (max([len(str(x)) for x in row.values()]) // 19)  # +1
    for _ in range(rows_required):
        rem = {k: str(row[k])[19:] for k in col_order}
        print(f"| {' | '.join(f'{rem[x][0:19]:^20}' for x in col_order)} |")


def print_title():
    print_seperator()
    print(f"| {' | '.join(f'{x:^20}' for x in cols).title()} |")
    print_seperator()


def print_seperator():
    p = '+'+'-'*22
    print(p*len(cols)+'+')


def list_all_bus(resource):
    cur = __get_all_bus(resource)
    print('Showing all buses')
    print_title()
    for row in cur.fetchall():
        print_row(row)
    print_seperator()
