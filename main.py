import logging

import mysql.connector

import bus
import queries
import reservation
from exceptions import AppException
from resource import Resource

logging.basicConfig(level=logging.INFO,
                    format='%(levelname)s: %(lineno)s: %(message)s ')  # https://realpython.com/python-logging/


def check_if_db_exists(resource, name):
    cur = resource.get_db_cursor()
    cur.execute('show databases')
    for each in cur.fetchall():
        if name in each:
            return True


def initialize(resource):
    if check_if_db_exists(resource, 'bus_reservation'):
        logging.debug('db exists')
        return
    logging.info(f'db does not exists, trying to create db {queries.database_name}')
    cur = resource.get_db_cursor()
    try:
        cur.execute(queries.create_db)
    except mysql.connector.Error as err:
        m = f'creating database {queries.database_name} failed: {err}'
        logging.critical(m)
        raise AppException(m)

    create_tables_queries = [queries.create_bus_info, queries.create_reservation_info]
    for query in create_tables_queries:
        try:
            cur.execute(query)
        except mysql.connector.Error as err:
            m = f'failed executing query {query}: {err}'
            logging.critical(m)
            raise AppException(m)


__menus__ = {
    1: ('initialize app', initialize),
    2: ('add bus', bus.add_bus),
    3: ('search bus', bus.search_bus),
    4: ('list all bus', bus.list_all_bus),
    5: ('reserv bus', reservation.add_reservation)
    # 3: ('quit', lambda _: exit(0))
}

__menus__[len(__menus__) + 1] = ('quit', lambda _: exit(0))


def display_menu():
    print('\n'.join(f'{k}: {v[0].title()}' for k, v in __menus__.items()))


def take_action(resource, menu):
    return __menus__[menu][1](resource)


if __name__ == '__main__':
    __resource = Resource()
    __resource.connect_db()
    i = 0
    stop = len(__menus__)+1
    while i != stop:
        display_menu()
        i = int(input('>> '))
        try:
            take_action(__resource, i)
        except AppException as e:
            logging.critical(e.message)
            print('')
    __resource.close_db_connection()
