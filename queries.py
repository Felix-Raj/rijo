# names
database_name = 'bus_reservation'
table_bus_info = 'bus_info'
table_reservation_info = 'reservation_info'

# create
# todo: primary keys are missing for some/all of the tables
create_db = f'create database {database_name} character set utf8'
create_bus_info = f'create table {database_name}.{table_bus_info} (name char(100) primary key, start char(100), ' \
                  f'stop char(100), fare int,seats int)'
create_reservation_info = f'create table {database_name}.{table_reservation_info} (' \
                          f'reservation_id int auto_increment primary key,' \
                          f'customer_name char(100),' \
                          f'seats int,' \
                          f'bus char(100),' \
                          f'foreign key(bus) references {database_name}.{table_bus_info}(name)' \
                          f')'

# select
select_bus_partial = f'select * from {database_name}.{table_bus_info} where '
select_all_bus = f'select * from {database_name}.{table_bus_info}'

# insertions
insert_into_bus = f'insert into {database_name}.{table_bus_info} (name, start, stop, fare, seats) values ' \
                  f'(%s, %s, %s, %s, %s)'
insert_into_reservation = f'insert into {database_name}.{table_reservation_info} ' \
                          f'(customer_name, seats, bus)' \
                          f'values (%s, %s, %s)'
