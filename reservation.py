import mysql.connector
import logging

import queries
from exceptions import AppException


def __add_reservation__(resource, customer_name, seats, bus):
    cur = resource.get_db_cursor()
    try:
        cur.execute(queries.insert_into_reservation, (customer_name, seats, bus))
        resource.cnx.commit()
    except mysql.connector.Error as err:
        m = f'error inserting data into {queries.table_reservation_info}\n' \
            f'query: {cur.statement}\n' \
            f'err: {err}'
        logging.error(m)
        raise AppException(m)


def add_reservation(resource):
    input_prompts = {
        'customer_name': 'customer name',
        'bus': 'bus',
        'seats': 'seats'
    }

    # todo: validate number of seats is actually available
    values = {}
    for k, v in input_prompts.items():
        values[k] = input(f'>> {v.title()}: ')
    __add_reservation__(resource, customer_name=values['customer_name'], seats=values['seats'], bus=values['bus'])
